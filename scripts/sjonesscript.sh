#!/bin/bash

# This script installs required packages and configures environment

# Install necessary packages
sudo yum install -y git vim

# Configure environment
sudo echo "export PATH=$PATH:/usr/local/bin" >> ~/.bashrc

