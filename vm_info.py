import socket
import platform

def get_hostname():
    return socket.gethostname()

def get_ip_address():
    hostname = get_hostname()
    return socket.gethostbyname(hostname)

def get_os_system():
    return platform.system()

def get_os_release():
    return platform.release()

def get_os_version():
    return platform.version()

def get_machine_architecture():
    return platform.machine()

def gather_vm_info():
    info = {
        "Hostname": get_hostname(),
        "IP Address": get_ip_address(),
        "OS System": get_os_system(),
        "OS Release": get_os_release(),
        "OS Version": get_os_version(),
        "Machine Architecture": get_machine_architecture()
    }
    return info

def save_to_file(info, filename):
    with open(filename, 'w') as file:
        for key, value in info.items():
            file.write("{}: {}\n".format(key, value))

def main():
    vm_info = gather_vm_info()
    output_file = "vm_info.txt"
    save_to_file(vm_info, output_file)
    print("Information saved to {}".format(output_file))

if __name__ == "__main__":
    main()

